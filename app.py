
import webapp2
import logging

from com.msb.web.handler import ErrorHandlerAdapter
from handlers import IndexHandler, InformationHandler, ContactHandler, Error404Handler, Error500Handler


application = webapp2.WSGIApplication([
    ('/', IndexHandler),
    ('/information', InformationHandler),
    ('/contact', ContactHandler),
], debug=True)


application.error_handlers[404] = ErrorHandlerAdapter(Error404Handler)
application.error_handlers[500] = ErrorHandlerAdapter(Error500Handler)