import os
import jinja2
import logging

from com.msb.web.handler import Handler
from forms import ContactForm


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__) + '/views'),
    extensions=['jinja2.ext.autoescape'])


class BaseHandler(Handler):

    def render_view(self, filename, template_values = {}):
        self.render_template(JINJA_ENVIRONMENT, filename, template_values)


class IndexHandler(BaseHandler):

    def get(self):
        self.render_view('index.html')


class InformationHandler(BaseHandler):

    def get(self):
        self.render_view('information.html')


class ContactHandler(BaseHandler):

    def get(self):

        contactForm = ContactForm()

        template_values = {
            'contactForm': contactForm,
            'errorDict': {}
        }

        self.render_view('contact.html', template_values)

    def post(self):

        contactForm = ContactForm(self.request)
        errorDict = contactForm.validate()

        view = 'contact_submit.html'

        if len(errorDict) > 0 :
            view = 'contact.html'
        else :
            contactForm.sendMessage()


        template_values = {
            'contactForm': contactForm,
            'errorDict': errorDict,
        }

        self.render_view(view, template_values)


class Error404Handler(BaseHandler):

    def get(self):

        exception=self.request.route_args['exception']

        self.response.set_status(404)
        self.render_view('error404.html')


class Error500Handler(BaseHandler):

    def get(self):

        exception=self.request.route_args['exception']

        self.response.set_status(500)
        self.render_view('error500.html')

