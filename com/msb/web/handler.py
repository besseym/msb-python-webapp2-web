
import webapp2
import logging

class Handler(webapp2.RequestHandler):

  def render_template(self, template_environment, filename, template_values = {}):
        template = template_environment.get_template(filename)
        self.response.write(template.render(template_values))


class ErrorHandlerAdapter(webapp2.BaseHandlerAdapter):

    def __call__(self, request, response, exception):

    	logging.exception(exception)

        request.route_args = {}
        request.route_args['exception'] = exception
        handler = self.handler(request, response)
        return handler.get()