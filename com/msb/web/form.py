import re
import cgi

class Form:

	def clean(self, value):
		return cgi.escape(value)

	def validateRequired(self, field, value, errorDict):
		if value == '':
			errorDict[field] = 'Field ' + field + ' is required'

	def validateMinLength(self, field, value, length, errorDict):
		if len(value) > length:
			errorDict[field] = 'Field ' + field + ' needs less than ' + length + ' characters in length'

	def validateMinLength(self, field, value, length, errorDict):
		if len(value) < length:
			errorDict[field] = 'Field ' + field + ' needs greater than ' + length + ' characters in length'

	def validateNumber(self, field, value, errorDict):
		if value.isdigit() != True :
			errorDict[field] = 'Field ' + field + ' needs to be a valid numerical value'

	def validateEmail(self, field, value, errorDict):
		if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", value) == None :
			errorDict[field] = 'Field ' + field + ' is not a valid email address.'
		

