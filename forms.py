from google.appengine.api import mail

from com.msb.web.form import Form

class ContactForm(Form):

	def __init__(self, request = None):

		if request != None :
			self.name = self.clean(request.get('name'))
			self.email = self.clean(request.get('email'))
			self.subject = self.clean(request.get('subject'))
			self.message = self.clean(request.get('message'))

	def validate(self):
		errorDict = {}
		
		self.validateRequired('name', self.name, errorDict)
		self.validateRequired('email', self.email, errorDict)
		self.validateRequired('subject', self.subject, errorDict)

		self.validateEmail('email', self.email, errorDict)

		return errorDict

	def sendMessage(self):
		message = mail.EmailMessage(sender="Administrator <besseym@gmail.com>", subject="New Contact Message")
		message.to = "Administrator <besseym@gmail.com>"
		message.body = """ 
		Name: """ + self.name + """
		Email : """ + self.email + """
		Message : 
		""" + self.message
		message.send()